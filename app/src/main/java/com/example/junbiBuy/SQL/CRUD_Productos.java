package com.example.junbiBuy.SQL;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class CRUD_Productos extends SQLiteOpenHelper {

    static SQLiteDatabase listasCompra;

    private static final String NOMBRE_DATABASE = "listasDeLaCompra.db";
    private static final int VERSION = 1;


    public CRUD_Productos(@Nullable Context context) {
        super(context, NOMBRE_DATABASE, null, VERSION);

    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        System.out.println(NOMBRE_DATABASE);
        this.listasCompra = sqLiteDatabase;

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
 }

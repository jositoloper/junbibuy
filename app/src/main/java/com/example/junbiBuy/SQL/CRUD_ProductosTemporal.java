package com.example.junbiBuy.SQL;




import static android.database.sqlite.SQLiteDatabase.openOrCreateDatabase;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import androidx.annotation.Nullable;

import com.example.junbiBuy.dto.ProductoDTO;

import java.util.ArrayList;

public class CRUD_ProductosTemporal extends SQLiteOpenHelper {

    SQLiteDatabase sqLiteDatabase;


    private static final String CREA_TABLA_TEMPORAL = "CREATE TABLE IF NOT EXISTS nombreListaReplace (marca VARCHAR(200), descripcion VARCHAR(200))";
    private static final String NOMBRE_DATABASE = "may.db";
    private static final int VERSION = 1;

    public CRUD_ProductosTemporal(@Nullable Context context) {
        super(context, NOMBRE_DATABASE, null, VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(CREA_TABLA_TEMPORAL);
        this.sqLiteDatabase = sqLiteDatabase;
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }



    public ArrayList<ProductoDTO> leeSQL(SQLiteDatabase databaseTemporal){

        ArrayList <ProductoDTO> listaTemporal = new ArrayList<>();

        Cursor myCursor = databaseTemporal.rawQuery("select marca, descripcion from nombreListaReplace", null);


        while(myCursor.moveToNext()) {
            String marca = myCursor.getString(0);
            String descripcion = myCursor.getString(1);

            ProductoDTO productoTemporal = new ProductoDTO(marca, descripcion);
            listaTemporal.add(productoTemporal);
        }
        myCursor.close();
        databaseTemporal.close();
        return listaTemporal;
    }


    public void borrar(Context context){
        context.deleteDatabase(NOMBRE_DATABASE);
    }

}

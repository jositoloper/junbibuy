package com.example.junbiBuy.SQL;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.junbiBuy.dto.ProductoDTO;

import java.util.ArrayList;

public class CRUD_Listas extends SQLiteOpenHelper {

    private static SQLiteDatabase listasCompra;



    private static final String NOMBRE_DATABASE = "listasDeLaCompra.db";
    private static final int VERSION = 1;


    public CRUD_Listas(@Nullable Context context) {
        super(context, NOMBRE_DATABASE, null, VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        this.listasCompra = sqLiteDatabase;

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void copiaTabla(SQLiteDatabase databaseTemporal, String nombreTabla){

        SQLiteDatabase databaseFirme = getWritableDatabase();
        Cursor myCursor = databaseTemporal.rawQuery("select marca, descripcion from nombreListaReplace", null);


        if(databaseFirme != null){
            while(myCursor.moveToNext()) {
                String marca = myCursor.getString(0);
                String descripcion = myCursor.getString(1);


                ProductoDTO productoTemporal = new ProductoDTO(marca, descripcion);
                ContentValues cv = new ContentValues();
                cv.put("marca", productoTemporal.getMarca());
                cv.put("descripcion", productoTemporal.getDescripcion());

                String CREA_TABLA = "CREATE TABLE IF NOT EXISTS " +nombreTabla+" (marca VARCHAR(200), descripcion VARCHAR(200))";


                databaseFirme.execSQL(CREA_TABLA);
                databaseFirme.insert(nombreTabla, null, cv);


            }
        }
        myCursor.close();
        databaseTemporal.close();
        databaseFirme.close();

    }

    public ArrayList<ProductoDTO> leeSQL(SQLiteDatabase sql, String nombreDataBaseLista){

        ArrayList <ProductoDTO> listaProductos = new ArrayList<>();

        Cursor myCursor = sql.rawQuery("SELECT * from "+ nombreDataBaseLista+"", null);

        while(myCursor.moveToNext()) {
            String marca = myCursor.getString(0);
            String descripcion = myCursor.getString(1);

            ProductoDTO productoDTO = new ProductoDTO(marca, descripcion);
            listaProductos.add(productoDTO);
        }
        myCursor.close();
       return listaProductos;
    }
 }

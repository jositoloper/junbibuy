package com.example.junbiBuy;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;

import com.example.junbiBuy.SQL.CRUD_Listas;
import com.example.junbiBuy.dto.ProductoDTO;

import java.util.ArrayList;

public class ProductosListasGuardadasActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private ArrayList<ProductoDTO> listaProductos;
    private RecyclerView recyclerView;
    private TextView nombreListaFirme;
    private CRUD_Listas crud_productos = new CRUD_Listas(this);
    private AdapterProductos adapterProductos = null;
    private TextView insertarProducto;
    private String nombreTablaSeleccionada;

    private SearchView buscador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productos_listas_guardadas);
        getSupportActionBar().hide();


        recyclerView = findViewById(R.id.recyclerViewProductosLista);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        nombreListaFirme = findViewById(R.id.nombreListaFirme);
        buscador = findViewById(R.id.buscar);
        insertarProducto = findViewById(R.id.insertarProducto);


        // ponemos a la escucha el buscador
        buscador.setOnQueryTextListener(this);

        insertarProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ProductosListasGuardadasActivity.this, EscaneaBuscaActivity.class);
                intent.putExtra("origen", 1);
                intent.putExtra("nombreTablaSeleccionada", nombreTablaSeleccionada);
                startActivity(intent);
            }
        });

        muestra();

    }

    private void muestra() {

        Intent intent = getIntent();
        try{
            nombreTablaSeleccionada =  intent.getStringExtra("nombreTablaSeleccionada");
        }catch(Exception e){

        }
        nombreListaFirme.setText(nombreTablaSeleccionada.replace("_", " "));
        SQLiteDatabase sql = crud_productos.getWritableDatabase();
        listaProductos = crud_productos.leeSQL(sql,nombreTablaSeleccionada);

        recyclerView.setAdapter(getAdapter());

    }

    private AdapterProductos getAdapter(){
        if(adapterProductos == null)
            adapterProductos = new AdapterProductos(listaProductos);

        return adapterProductos;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            salirLista();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void salirLista() {

        new AlertDialog.Builder(this)
                .setIcon(R.drawable.icono)
                .setTitle("Si sale perderá su avance...")
                .setCancelable(false)
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {// un listener que al pulsar, cierre la aplicacion
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(ProductosListasGuardadasActivity.this, GestionaListaActivity.class);
                        startActivity(intent);
                    }
                }).show();
    }

    // metodos por implementar la interfaz SearchViewListener


    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {

        adapterProductos.busqueda(s);
        return false;
    }
}
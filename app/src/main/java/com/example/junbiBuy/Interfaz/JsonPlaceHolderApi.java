package com.example.junbiBuy.Interfaz;

import com.example.junbiBuy.Modelo.Posts;

import retrofit2.Call;
import retrofit2.http.GET;

public interface JsonPlaceHolderApi {

    @GET("product")
    Call<Posts> getPosts();


}

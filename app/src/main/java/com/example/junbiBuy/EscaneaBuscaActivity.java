package com.example.junbiBuy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.junbiBuy.Interfaz.JsonPlaceHolderApi;
import com.example.junbiBuy.Modelo.Posts;
import com.example.junbiBuy.SQL.CRUD_Productos;
import com.example.junbiBuy.dto.ProductoDTO;
import com.example.junbiBuy.SQL.CRUD_ProductosTemporal;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EscaneaBuscaActivity extends AppCompatActivity {



    private Button escanea, listar;
    private TextView marca, descripcion;
    private IntentResult result;
    private Retrofit retrofit;
    private FirebaseFirestore db;
    private String numeroCodigo;

    CRUD_ProductosTemporal crud_productosTemporal = new CRUD_ProductosTemporal(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escanea_busca);
        getSupportActionBar().hide();


        marca = findViewById(R.id.marca);
        descripcion = findViewById(R.id.descripcion);


        escanea = findViewById(R.id.scan);
        escanea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                IntentIntegrator integrador = new IntentIntegrator(EscaneaBuscaActivity.this);
                integrador.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
                integrador.setPrompt("junbiBuy");
                integrador.setCameraId(0);
                integrador.setBeepEnabled(true);
                integrador.setBarcodeImageEnabled(true);
                integrador.initiateScan();
                integrador.setOrientationLocked(false);

            }
        });

        listar = findViewById(R.id.anideALista);

        listar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String isMarca = (String) marca.getText();
                String isDescripcion = (String) descripcion.getText();

                if(isMarca.equals("NOMBRE DE LA MARCA") && isDescripcion.equals("DESCRIPCION")){
                    Toast.makeText(EscaneaBuscaActivity.this, "Debe escanear un producto", Toast.LENGTH_LONG).show();

                }else{

                    // Aqui valoramos quien hace la llamada para escanear, si la lista firme o la lista temporal.
                    // Valoramos con un try catch por si la llamada es de lista temporal, ya que el getExtras es null
                    int origenLlamada = 0;
                    String nombreTablaSeleccionada = null;

                    try {

                        origenLlamada = getIntent().getExtras().getInt("origen");
                        nombreTablaSeleccionada = getIntent().getExtras().getString("nombreTablaSeleccionada");

                    }catch(Exception e ){
                        e.printStackTrace();
                    }

                    if(origenLlamada == 1){

                        ProductoDTO productoParaListar = new ProductoDTO((String)marca.getText(),(String) descripcion.getText() );
                        insertaEnTablaFirmeSQL(productoParaListar, nombreTablaSeleccionada);

                    }else{ // aqui creamos un producto con los datos escaneados
                        ProductoDTO productoParaListar = new ProductoDTO((String)marca.getText(),(String) descripcion.getText() );

                        // aqui lo guardamos en la base de datos
                        gestionaSQL(productoParaListar);

                        // aqui leemos de la base de datos la lista de los productos que hemos ido metiendo para enviarlos en un bundle a la activity que los lista
                        SQLiteDatabase databaseTemporal = crud_productosTemporal.getReadableDatabase();
                        ArrayList <ProductoDTO> listaTemp = crud_productosTemporal.leeSQL(databaseTemporal);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("productoEnLista", listaTemp);

                        Intent intent = new Intent(EscaneaBuscaActivity.this, CreaListaActivity.class);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }

                }

            }
        });
    }

    protected void onActivityResult(int requiestCode, int resultCode, Intent data) {

        result = IntentIntegrator.parseActivityResult(requiestCode, resultCode, data);

        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, "Lectura cancelada", Toast.LENGTH_LONG).show();
            } else {
                numeroCodigo = result.getContents();
                getBusqueda(marca, descripcion, result.getContents());
            }
        } else {
            super.onActivityResult(requiestCode, resultCode, data);
        }
    }

    private void getBusqueda(TextView marca, TextView descripcion, String numeroCodigo) {

        numeroCodigo = " /" + numeroCodigo + "/";
        retrofit = new Retrofit.Builder().baseUrl("http://world.openfoodfacts.org/api/v0/" + numeroCodigo)
                .addConverterFactory(GsonConverterFactory.create()).build();

        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<Posts> call = jsonPlaceHolderApi.getPosts();
        call.enqueue(new Callback<Posts>() {
            public void onResponse(Call<Posts> call, Response<Posts> response) {
                Posts posts = response.body();

                try {

                    // en este if se valora si en open food esta registrado el codigo de barras pero no tiene informacion, si es asi
                    // se pasa directamente a registrar el producto a Firebase
                    if(posts.getProducto().getBrands().equals("") || posts.getProducto().getBrands() == null)
                        if(posts.getProducto().getProduct_name().equals("") || posts.getProducto().getProduct_name() == null){
                            buscaFirebase();
                        }
                    marca.setText(posts.getProducto().getBrands());
                    descripcion.setText(posts.getProducto().getProduct_name());
                } catch (Exception e) {

                    buscaFirebase();
                }
            }
            public void onFailure(Call<Posts> call, Throwable t) {
                Toast.makeText(EscaneaBuscaActivity.this, "FALLO DE CONEXION", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void insertaActivity() {
        Intent intent = new Intent(this, InsertaFirebaseActivity.class);
        intent.putExtra("codigoBarras", numeroCodigo);
        startActivity(intent);
    }

    private void buscaFirebase() {

        db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("product").document(numeroCodigo);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {

                        String marcaFire = document.getData().get("marca") + "";
                        String descripcionFire = document.getData().get("descripcion") + "";
                        marca.setText(marcaFire);
                        descripcion.setText(descripcionFire);

                    } else {
                        insertaActivity();
                    }

                } else {

                }
            }
        });
    }


    private void gestionaSQL(ProductoDTO productoParaListar){

        CRUD_ProductosTemporal crud_productosTemporal = new CRUD_ProductosTemporal(this);
        SQLiteDatabase databaseTemporal = crud_productosTemporal.getWritableDatabase();
        String marca= productoParaListar.getMarca();
        if(databaseTemporal!= null){
            ContentValues cv = new ContentValues();
            cv.put("marca", productoParaListar.getMarca());
            cv.put("descripcion", productoParaListar.getDescripcion());
            databaseTemporal.insert("nombreListaReplace", null, cv);
        }

    }

    private void insertaEnTablaFirmeSQL(ProductoDTO productoParaListar, String nombreTablaSeleccionada){


        CRUD_Productos crud_productos = new CRUD_Productos(this);
        SQLiteDatabase databaseFirme = crud_productos.getWritableDatabase();

        if(databaseFirme!= null){
            ContentValues cv = new ContentValues();
            cv.put("marca", productoParaListar.getMarca());
            cv.put("descripcion", productoParaListar.getDescripcion());
            databaseFirme.insert( nombreTablaSeleccionada, null, cv);
        }


        // una vez finalizada la insercion del producto en la lista firme, pasamos al activity que muestra los productos,
        // pasandole el nombre de la tabla en la que estabamos para mostrar los productos

        Intent intent = new Intent(this, ProductosListasGuardadasActivity.class);
        intent.putExtra("nombreTablaSeleccionada", nombreTablaSeleccionada);
        startActivity(intent);

    }



}
package com.example.junbiBuy;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.junbiBuy.dto.ProductoDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AdapterProductos extends RecyclerView.Adapter <AdapterProductos.ViewHolderProductos> implements View.OnClickListener  {

    private ArrayList<ProductoDTO> productos;
    private View.OnClickListener listener;

    private ArrayList <ProductoDTO> productosBuscador;
    private ArrayList <ProductoDTO> productosReserva;

    public AdapterProductos( ArrayList<ProductoDTO> productos) {

        this.productos = productos;
        this.productosBuscador = new ArrayList<>();
        this.productosReserva = new ArrayList<>();
        productosReserva.addAll(productos);
    }

    @NonNull
    @Override
    public ViewHolderProductos onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_productos, null, false);

        view.setOnClickListener(this);

        return new ViewHolderProductos(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderProductos holder, int position) {
        holder.asignarDatos(productos.get(position));
    }


    public void busqueda (String buscar){

        if(buscar.length() == 0){
            productos.clear();
            productos.addAll(productosReserva);
        }else{

            productosBuscador.clear();

            for(ProductoDTO productoFiltra : productosReserva){
                if(productoFiltra.getDescripcion().toLowerCase().contains(buscar.toLowerCase()))
                    productosBuscador.add(productoFiltra);
            }
            productos.clear();
            productos.addAll(productosBuscador);

        }
        notifyDataSetChanged();
    }

    // listener al pulsar adapter productos

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if(listener != null){
            listener.onClick(view);
        }
    }

    @Override
    public int getItemCount() {
        return productos.size();
    }




    public class ViewHolderProductos extends RecyclerView.ViewHolder{

        private TextView descripcion;
        private TextView descripcionColor;

        private TextView marcaProducto;
        private TextView marcaProductoColor;
        private int estadoColor = 1;



        public ViewHolderProductos(@NonNull View itemView) {
            super(itemView);
            descripcion = (TextView) itemView.findViewById(R.id.idDato);
            marcaProducto = (TextView) itemView.findViewById(R.id.marcaProducto);

            descripcionColor = (TextView) itemView.findViewById(R.id.idDato);
            marcaProductoColor = (TextView) itemView.findViewById(R.id.marcaProducto);

            descripcionColor.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    if(estadoColor%2 != 0) {
                        descripcionColor.setTextColor(Color.rgb(176,124,248));
                        marcaProductoColor.setTextColor(Color.rgb(176,124,248));
                        estadoColor++;
                    }
                    else{
                        descripcionColor.setTextColor(Color.BLACK);
                        marcaProductoColor.setTextColor(Color.BLACK);
                        estadoColor++;
                    }

                }
            });
        }

        public void asignarDatos(ProductoDTO producto) {
            descripcion.setText(producto.getDescripcion());
            marcaProducto.setText(producto.getMarca());
        }


    }


}
package com.example.junbiBuy;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;


import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.junbiBuy.dto.ProductoDTO;
import com.example.junbiBuy.R;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;


import java.util.HashMap;
import java.util.Map;

public class InsertaFirebaseActivity extends AppCompatActivity {


    private TextView codigoNueva;
    private TextView marcaNueva;
    private TextView descripcionNueva;
    private Button botonEnviar;
    private FirebaseFirestore db;
    private Map<String, String> listaProducto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inserta_datos);
        getSupportActionBar().hide();


        codigoNueva = findViewById(R.id.codigoNueva);
        marcaNueva = findViewById(R.id.marcaNueva);
        descripcionNueva = findViewById(R.id.descripcionNueva);
        botonEnviar =  findViewById(R.id.botonEnviar);
        listaProducto = new HashMap<>();


        Bundle bundle = this.getIntent().getExtras();
        String dato = bundle.getString("codigoBarras");
        codigoNueva.setText(dato);

        botonEnviar.setOnClickListener(view -> {

            String marca = marcaNueva.getText().toString();
            String descripcion = descripcionNueva.getText().toString();

            enviarFirebase(dato, marca, descripcion);

        });
    }

    public void enviarFirebase(String codigo, String marca, String descripcion){

        ProductoDTO productoNuevo = new ProductoDTO(marca, descripcion);

        System.out.println(codigoNueva);

        listaProducto.put("marca", productoNuevo.getMarca().toString());
        listaProducto.put("descripcion", productoNuevo.getDescripcion().toString());

        db = FirebaseFirestore.getInstance();

        db.collection("product").document(""+codigo).set(listaProducto, SetOptions.merge());

        Toast.makeText(InsertaFirebaseActivity.this, "Dato enviado", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent (InsertaFirebaseActivity.this, EscaneaBuscaActivity.class);
        startActivity(intent);
    }
}
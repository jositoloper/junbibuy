package com.example.junbiBuy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;

import com.example.junbiBuy.SQL.CRUD_Listas;

import java.util.ArrayList;

public class GestionaListaActivity extends AppCompatActivity {

    private RecyclerView listasCompra;
    private CRUD_Listas listasFirmes = new CRUD_Listas(GestionaListaActivity.this);
    private AdapterListas adapterListas;
    private ArrayList <String> listas;
    private SQLiteDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestiona_lista);
        getSupportActionBar().hide();

        listasCompra = findViewById(R.id.recyclerListas);
        listasCompra.setLayoutManager(new LinearLayoutManager(this));


        registerForContextMenu(listasCompra);

        muestraDatosListas();

    }

    /// Este metodo es el encargado de mostar el menu contextual y establecer la accion seleccionada

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
         getMenuInflater().inflate(R.menu.menu_contexto, menu);

    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {

        if(item.getItemId() == R.id.eliminarLista) {

            AlertDialog.Builder builder = new AlertDialog.Builder(GestionaListaActivity.this);

            builder.setTitle("Eliminar Lista").setMessage("¿Desea eliminar la lista?").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    int posicion = adapterListas.getPosicion();
                    String nombre = listas.get(posicion).toString().replace(" ", "_");
                    database.execSQL("DROP TABLE IF EXISTS " + nombre);
                    adapterListas.notifyItemRemoved(posicion);
                    muestraDatosListas();
                }
            }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            }).create().show();
        }

        return false;
    }

    /// Este metodo muestra las tablas creadas para cada lista de la compra
    // y si pulsamos sobre una lista, se muestran los productos de la misma

    private void muestraDatosListas() {
        database = listasFirmes.getReadableDatabase();
        listas = new ArrayList<>();

        Cursor myCursor = database.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name!='android_metadata'", null);
        while (myCursor.moveToNext()) {
            String nombreLista = myCursor.getString(0).replaceAll("_", " ");
            listas.add(nombreLista);
        }

        adapterListas = new AdapterListas(listas);
        listasCompra.setAdapter(adapterListas);

        adapterListas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int posicion = listasCompra.getChildAdapterPosition(view);
                String nombre = listas.get(posicion).toString().replace(" ", "_");

                Intent intent = new Intent(GestionaListaActivity.this, ProductosListasGuardadasActivity.class);
                intent.putExtra("nombreTablaSeleccionada", nombre);
                startActivity(intent);

            }
        });

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(GestionaListaActivity.this, MenuActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}
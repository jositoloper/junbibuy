package com.example.junbiBuy;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;

public class AdapterListas extends RecyclerView.Adapter <AdapterListas.ViewHolderListas> implements View.OnClickListener {

    private ArrayList<String> listasCompra;
    private View.OnClickListener listener;
    private ViewHolderListas viewHolderListas;
    private int posicion;

    public AdapterListas(ArrayList<String> listasCompra) {

        this.listasCompra = listasCompra;

    }

    @NonNull
    @Override
    public ViewHolderListas onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_listas, null, false);

        view.setOnClickListener(this);

        return new ViewHolderListas(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderListas holder, int position) {
        holder.asignarDatos(listasCompra.get(position));
    }


    @Override
    public int getItemCount() {

        return listasCompra.size();

    }

    public class ViewHolderListas extends RecyclerView.ViewHolder {

        TextView descripcion;



        public ViewHolderListas(@NonNull View itemView) {
            super(itemView);
            descripcion = (TextView) itemView.findViewById(R.id.idDatos);

        /// CON EL METODO SETONLONG... ESTABLECEMOS EL CONTEXTMENU EN LA CLASE GESTIONALISTA QUE IMPLEMENTA EL ADAPTER.
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                public boolean onLongClick(View v) {
                    posicion = getAdapterPosition();
                    return false;
                }
            });
        }

        public void asignarDatos(String listasCompra) {
            System.out.println(getItemCount());
            String dime = listasCompra.toString();
            descripcion.setText(listasCompra.toString());
        }

    }

    // listener al pulsar adapter listas

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }


    @Override
    public void onClick(View view) {
        if(listener != null){
            listener.onClick(view);
        }
    }

    // Mediante este metodo obtenemos la posicion del elemento pulsado en el context menu
    public int getPosicion(){
        return posicion;
    }


}
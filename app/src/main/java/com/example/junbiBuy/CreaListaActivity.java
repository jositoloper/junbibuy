package com.example.junbiBuy;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.junbiBuy.SQL.CRUD_Productos;
import com.example.junbiBuy.dto.ProductoDTO;
import com.example.junbiBuy.SQL.CRUD_Listas;
import com.example.junbiBuy.SQL.CRUD_ProductosTemporal;

import java.util.ArrayList;

public class CreaListaActivity extends AppCompatActivity {

    private ArrayList <ProductoDTO> listaProductos;
    private EditText nombreLista;
    private RecyclerView listViewProductos;
    private Button aniadir, finalizar;
    private static String nombreTablaDataBaseLista;
    private CRUD_ProductosTemporal crud_productosTemporal = new CRUD_ProductosTemporal(this);
    private CRUD_Productos crud_productos = new CRUD_Productos(this);
    private CRUD_Listas crud_listas = new CRUD_Listas(this);
    private ImageView atras;
    private ArrayList <String> tablas;
    private ArrayList <ProductoDTO> listaTemp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crea_lista);
        getSupportActionBar().hide();

        muestraDatosTemporales();

        listViewProductos = (RecyclerView) findViewById(R.id.listViewProductos);
        listViewProductos.setLayoutManager(new LinearLayoutManager(this));
        nombreLista = (EditText) findViewById(R.id.nombreLista);
        aniadir = (Button) findViewById(R.id.aniadeProducto);
        finalizar = (Button) findViewById(R.id.finalizaLista);
        atras = findViewById(R.id.atras);


        // leer abajo la explicacion de este metodo
        aplicaNombreLista();


        // El boton finalizar esta a la escucha para finalizar la lista. Una vez terminada la lista, es requerido un nombre para la misma
        // ya que será el nombre de la base de datos de la lista final. Se elimina la lista temporal y pasa a ser una lista en firme

        finalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                gestionaFinalizar();


            }
        });

        aniadir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // leer abajo la explicacion de este metodo
                guardaNombreLista();

                Intent intent = new Intent(CreaListaActivity.this, EscaneaBuscaActivity.class);
                startActivity(intent);

            }
        });


        atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CreaListaActivity.this, MenuActivity.class);
                startActivity(intent);
            }
        });

        leer();

    }

    private void gestionaFinalizar(){
        nombreTablaDataBaseLista = (String) nombreLista.getText().toString();

        CRUD_Listas crud_listaFirme = new CRUD_Listas(CreaListaActivity.this);

        if(nombreTablaDataBaseLista.matches("^[0-9]")) {
            Toast.makeText(CreaListaActivity.this, "La lista no puede comenzar por un numero", Toast.LENGTH_SHORT).show();
        }else {

            if (!nombreTablaDataBaseLista.equals("")) {

                /// SI LA LISTA DE PRODUCTOS TEMPORALES ESTA VACIA NO PERMITE FINALIZAR LA LISTA

                if (listaTemp == null || listaTemp.size() <= 0) {


                    Toast.makeText(CreaListaActivity.this, "Debe insertar al menos un producto en la lista", Toast.LENGTH_SHORT).show();

                } else {

                    // SI EL NOMBRE DE LA LISTA YA EXISTE PREGUNTA SI DESEAS GUARDAR LOS PRODUCTOS GRABADOS EN ESA LISTA

                    if (existeTabla(nombreTablaDataBaseLista)) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(CreaListaActivity.this);

                        builder.setTitle("Ya existe una lista con ese nombre")
                                .setMessage("¿Desea agregar los productos a esa lista?")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                        SQLiteDatabase databaseTemporal = crud_productosTemporal.getReadableDatabase();
                                        crud_listaFirme.copiaTabla(databaseTemporal, dameNombre());
                                        crud_productosTemporal.borrar(CreaListaActivity.this);

                                        nombreLista.setText("");
                                        guardaNombreLista();
                                        Intent intent = new Intent(CreaListaActivity.this, MenuActivity.class);
                                        startActivity(intent);

                                    }
                                }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Toast.makeText(CreaListaActivity.this, "Asigne otro nombre a su lista", Toast.LENGTH_SHORT).show();
                            }
                        }).create().show();

                    } else {

                        SQLiteDatabase databaseTemporal = crud_productosTemporal.getReadableDatabase();
                        crud_listaFirme.copiaTabla(databaseTemporal, dameNombre());
                        crud_productosTemporal.borrar(CreaListaActivity.this);

                        nombreLista.setText("");
                        guardaNombreLista();
                        Intent intent = new Intent(CreaListaActivity.this, MenuActivity.class);
                        startActivity(intent);
                    }
                }
            } else {
                Toast.makeText(CreaListaActivity.this, "Debe asignar un nombre a su lista", Toast.LENGTH_SHORT).show();
            }
        }
    }


    // Este metodo se encarga de leer la base de datos temporal a medida que vamos insertando productos en la lista temporal
    private void leer (){

        Bundle bundle = this.getIntent().getExtras();
        if(bundle != null){
            listaProductos = (ArrayList<ProductoDTO>) bundle.getSerializable("productoEnLista");
            AdapterProductos adapterProductos = new AdapterProductos(listaProductos);
            listViewProductos = findViewById(R.id.listViewProductos);
            listViewProductos.setAdapter(adapterProductos);
        }
    }

    // Este metodo se encarga de presentar la lista de preparacion al arrancar la app.
    private void muestraDatosTemporales(){
        SQLiteDatabase databaseTemporal = crud_productosTemporal.getReadableDatabase();
        listaTemp = crud_productosTemporal.leeSQL(databaseTemporal);
        AdapterProductos adapterProductos = new AdapterProductos(listaTemp);
        listViewProductos = findViewById(R.id.listViewProductos);
        listViewProductos.setAdapter(adapterProductos);
    }

    private static String dameNombre(){
        if(nombreTablaDataBaseLista == null)
            return nombreTablaDataBaseLista;
        nombreTablaDataBaseLista = nombreTablaDataBaseLista.replaceAll(" ", "_");

        return nombreTablaDataBaseLista;
    }


    // este metodo GUARDA el nombre de la lista temporal que estamos creando, para no tener que estar
    // continuamente escribiendolo en el cambio de activities
    private void guardaNombreLista(){

        SharedPreferences sharedPreferences = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("nombreListaParaGuardar", nombreLista.getText().toString());

        editor.apply();
    }


    // este metodo RECUPERA el nombre de la lista temporal que estamos creando, para no tener que estar
    // continuamente escribiendolo en el cambio de activities
    private void aplicaNombreLista() {

        SharedPreferences sharedPreferences = getPreferences(Context.MODE_PRIVATE);

        String pordefecto = null;

        nombreLista.setText(sharedPreferences.getString("nombreListaParaGuardar", pordefecto));

    }


    // Este metodo comprueba si en la base de datos de listas existe una tabla con ese nombre,
    // devuelve true si es asi, false si no existe
    private boolean existeTabla(String nombreTabla){

        tablas = new ArrayList<>();

        SQLiteDatabase listasCompraDatabase = crud_listas.getReadableDatabase();

        Cursor myCursor = listasCompraDatabase.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name!='android_metadata'", null);

        while (myCursor.moveToNext()) {
            String nombreLista = myCursor.getString(0).replaceAll("_", " ");
            tablas.add(nombreLista);
        }

        for(String comparaTabla : tablas){
            if(comparaTabla.equals(nombreTabla))
                return true;
        }
        return false;
    }


    // metodo que al hacer click sobre el boton atras del dispositivo te manda a menu directamente
    // con esto evito que retorne por la pila de activities

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(CreaListaActivity.this, MenuActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


}
package com.example.junbiBuy.Modelo;


import com.google.gson.annotations.SerializedName;

public class Posts {

    @SerializedName("product")
    private ProductoNombre producto;

    public Posts(ProductoNombre producto) {
        this.producto = producto;
    }

    public ProductoNombre getProducto() {
        return producto;
    }

    public void setProduct_name(ProductoNombre product_name) {
        this.producto = producto;
    }
}

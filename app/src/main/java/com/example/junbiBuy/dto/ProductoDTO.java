package com.example.junbiBuy.dto;

import java.io.Serializable;

public class ProductoDTO implements Serializable {


    String marca;
    String descripcion;

    public ProductoDTO(String marca, String descripcion) {

        this.marca = marca;
        this.descripcion = descripcion;
    }



    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "ProductoDTO{" +
                "marca='" + marca + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }
}

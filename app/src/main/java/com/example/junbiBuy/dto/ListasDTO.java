package com.example.junbiBuy.dto;

import java.io.Serializable;

public class ListasDTO implements Serializable {

    String nombreLista;

    public ListasDTO (String nombreLista){

        this.nombreLista = nombreLista;
    }

    public String getNombreLista() {
        return nombreLista;
    }

    public void setNombreLista(String nombreLista) {
        this.nombreLista = nombreLista;
    }

    @Override
    public String toString() {
        return "ListasDTO{" +
                "nombreLista='" + nombreLista + '\'' +
                '}';
    }
}

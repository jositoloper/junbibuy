package com.example.junbiBuy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.airbnb.lottie.LottieAnimationView;

public class MainSplashActivity extends AppCompatActivity {

    LottieAnimationView imagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();

        imagen = (LottieAnimationView) findViewById(R.id.animacion);
        imagen.setAnimation(R.raw.barcode3);
        imagen.playAnimation();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainSplashActivity.this, MenuActivity.class);
                startActivity(intent);
            }
        }, 2800);
    }
}